# NotPlayer

NotPlayer is a beautiful music player developed with [Tauri](https://tauri.app/), [Vue3](https://vuejs.org/), and [Vuetify](https://vuetifyjs.com/).

## Features

*Todo List*

- [ ] Play Local Musics
- [ ] Supports Online Music
    - [ ] NetEase Cloud Music

## Develop

First, Clone the storage.

Next, Install Node.js and Tauri for develop.

Use `yarn` to setup workspace. (You can also use other package managers)

Use `yarn tauri dev` or `cargo tauri dev` to start preview.

If you get any problems/ideas, write a issue to us.

## License

NotPlayer is Licensed under the MIT License.
